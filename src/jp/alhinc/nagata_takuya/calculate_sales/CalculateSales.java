package jp.alhinc.nagata_takuya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales{
	public static void main(String[] args) {

		BufferedReader br = null;
		String line;
		String folderPath = args[0];

		//支店定義ファイル
		HashMap<String, String> branchmap = new HashMap<>();
		//売り上げファイル
		HashMap<String, Long> salesmap = new HashMap<>();
		//ファイル呼び出し用【支店定義ファイルと支店別集計ファイル】
		File file = new File(args[0], "branch.lst");
		File fileOut = new File(args[0], "branch.out");

		try {
			//支店定義ファイルがない場合のエラー処理
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			//支店定義ファイルと売り上げファイルの読み込みとマップへの追加
			while ((line = br.readLine()) != null) {
			//支店定義ファイルの支店番号のみの場合のエラー表示
				if(!(line.matches("^[0-9]{3},[^,]+" ))) {
					System.out.println("支店定義ファイルのフォーマットが不正です。");
					return;
				}
				String[] branch = line.split(",", 0);
				branchmap.put(branch[0], branch[1]);
				salesmap.put(branch[0], 0L);

				if(!branch[0].matches(("^[0-9]{3}" ))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
			}
		} catch (IOException e) { //例外が発生した場合の処理
			System.out.println("予期せぬエラーが発生しました");
		} finally { //例外の有無に関わらず必ず実行される処理
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}

		try {
			//8桁の売り上げファイルの抽出と.rcdファイルの抽出
			FilenameFilter fileFilter = new FilenameFilter() {
				public boolean accept(File file, String filename) {
					if (filename.matches("[0-9]{8}\\.(rcd)$") && new File(file, filename).isFile()) {
						return true;
					} else {
						return false;
					}
				}
			};

			File[] saleFiles = new File(args[0]).listFiles(fileFilter);
			int checkError = Integer.parseInt(saleFiles[0].getName().replace(".rcd", ""));

			//連番になっていないときのエラー処理
			for (int s = 0; s < saleFiles.length; s++) {
				if (Integer.parseInt(saleFiles[s].getName().replace(".rcd", "")) != checkError + s) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}
			for (int i = 0; i < saleFiles.length; i++) {
				File fileRead = new File(folderPath, saleFiles[i].getName());
				FileReader salesLine = new FileReader(fileRead);

				//売り上げファイルの読み込み
				br = new BufferedReader(salesLine);

				//1行ずつ読み込み
				String code = br.readLine();
				Long amount = Long.parseLong(br.readLine());
				String emptyLine = br.readLine();

				//売り上げファイルの該当番号がなかった際のエラー処理
				if (!salesmap.containsKey(code)) {
					System.out.println(saleFiles[i].getName() + "の支店コードが不正です");
					salesLine.close();
					return;
				}
				//売り上げに加算してmapに保持をする
				salesmap.put(code, amount + (salesmap.get(code)));

				//売り上げファイルが3行以上のコードの記述がある場合のエラー処理
				if (emptyLine != null) {
					System.out.println(saleFiles[i].getName() + "のフォーマットが不正です");
					salesLine.close();
					return;
				}
				//売り上げファイルの額が10桁を超えた際のエラー処理
				Long exceedAmount = salesmap.get(code);
				if (exceedAmount.toString().length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					salesLine.close();
					return;
				}
			}

		} catch (IOException e) { //例外が発生した場合の処理
			System.out.println("予期せぬエラーが発生しました");
		} finally { //例外の有無に関わらず必ず実行される処理
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}

		//ファイルの書き込み操作
		try {
			// FileWriterクラスのオブジェクトを作成する
			FileWriter filewrite = new FileWriter(fileOut);
			// PrintWriterクラスのオブジェクトを作成する
			PrintWriter pw = new PrintWriter(new BufferedWriter(filewrite));

			for (Map.Entry<String, String> addsales : branchmap.entrySet()) {
			//作成したファイルに書き込む
				pw.println(addsales.getKey() + "," + addsales.getValue() + "," + salesmap.get(addsales.getKey()));
			}
			//ファイルを閉じる
			pw.close();
		} catch (IOException e) { //例外が発生した場合の処理
			System.out.println("予期せぬエラーが発生しました");
		} finally { //例外の有無に関わらず必ず実行される処理
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
	}
}